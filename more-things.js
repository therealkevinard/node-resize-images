/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Just... a bunch of fun es6 things to play with
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
----------------------------------------------------------- Spread Operator
 */

/**
 * Fn that takes 3 args.
 */
const fnWithArray = (a, b, c) => {
    console.log(a, b, c);
}

/**
 * Fn that takes 6 args
 */
const fnWithMixedArray = (a, b, c, d, e, f) => {
    console.log(a, b, c, d, e, f);
}

// an array with 3 elems
let arr = ['foo', 'bar', 'baz'];

fnWithArray(...arr); // <-- see what we did there? ;) spread exploded the arr into the 3 args
fnWithMixedArray('one', ...arr, 'five', 'six'); // <-- yep. works that way too.

/*
----------------------------------------------------------- Rest Operator - kind of the opposite of what we did ^-- up there --^
 */
/**
 * Where spreading exploded the arr into its elems, rest will accept [n] args, which are then passed to your fn body as an arr
 */
const addABunchOfNumbers = (...someArguments) => {
    // using `.reduce()` sum the inderminate arr
    let sum = someArguments.reduce((prev, curr) => {
        return prev + curr;
    }, 0);
    console.log(`sum is: ${sum}`);
};

addABunchOfNumbers(1, 2, 3, 4, 5, 6, 7, 8, 9);
addABunchOfNumbers(1, 5, 10);

/**
 * Nothing fancy - just gives you a number
 */
const getANumber = (seed) => {
    return Math.floor(Math.random() * seed)
}

/*
        some witchcraft
 */

// build a big array of big numbers
let randArr = [];
for(let i=0; i<getANumber(200); i++){
    randArr.push(getANumber(1000));
}
// the fn uses rest, which allows [n] args. we can *also* use spread to break our [n] arr to [n] args.
addABunchOfNumbers(...randArr);

/*
----------------------------------------------------------- Sure - more destructuring
*/

/*
        destructure variable assignments
 */
let a, b, c, d, e;
[a, b, c] = [10, 20, 30];
[d, e] = [40, 50];

const checkNumbers = (...someNumbers) => {
    console.log(someNumbers);
};

checkNumbers(a, b, c, d, e);

/*
        destructuring objects (this is fun)
 */
// make an object with known props (foo, bar, baz)
let someObject = {
    foo: 'a foo thing',
    bar: 'a bar thing',
    baz: 'a baz thing',
};

/*
destruc. into vars foo & bar - since foo and bar are props in the object, they're assigned. baz is ignored.
this is roughly equiv to
```
var foo = obj.foo (or obj[foo])
var bar = obj.bar (or obj[bar])
```
 */
var {
    foo,
    bar
} = someObject;
console.log(foo, bar);


/*
Similar to the vars above, this fn signature [ note the ({var...}) => {} ] allows you to pass an object as
its argument - the object's props/keys will be available as vars in your method body.
(you'll see this a whooooooooole lot when you start playing with react ;) )
 */
const destructureInFnParams = ({bar, baz}) => {
    console.log(bar, baz);
}

destructureInFnParams(someObject);