const imagemin = require('imagemin');
const ImageminGm = require('imagemin-gm');
const Imagemin_pngToJpeg = require('png-to-jpeg');
const Imagemin_jpgMoz = require('imagemin-mozjpeg');
const Imagemin_pngQuant = require('imagemin-pngquant');
const dirTree = require('directory-tree');

const imagemin_gm = new ImageminGm();
const imPlugins = [
    imagemin_gm.resize({
        width: 773,
        gravity: 'Center'
    }),
    Imagemin_jpgMoz({
        quality: 70,
    }),
    Imagemin_pngQuant({
        quality: '100',
        speed: 1,
        posterize: 0,
        floyd: 1
    })
];

//---------------------------------------------------------------- Helpers
/**
 * Given a tree node, return children that match the type
 * @param node
 * @param type
 * @return {*}
 */
const getChildrenByType = (node, type) => {
    return node.children.filter(child => {
        return child.type === type;
    })
};

//---------------------------------------------------------------- Main Recursive handler
/**
 *
 * @param tree
 * @return {Promise<void>}
 */
const recurseNode = async (tree) => {
    const childFiles = getChildrenByType(tree, 'file');
    const childDirs = getChildrenByType(tree, 'directory');

    let patterns = {
        in: `${tree.path}/*.{jpg,png}`, //-- matches imgs in cwd
        out: `dist/${tree.path.replace('src', '')}` //-- preserve path in dist dire
    };

    try {
        let done = await imagemin_optimize(patterns.in, patterns.out);
    }
    catch (e) {
        console.error(e);
        debugger;
    }

    //-- just illustrating map and things like that.
    childFiles.map(file => {
        // do something with a file
        console.log(`found ${file.path}`)
    })
    childDirs.map(async dir => {
        console.log(`----- in ${dir.path}`);
        await recurseNode(dir); // <---------- the self-recursing part.
    })
};

const tree = dirTree('src', {
    extensions: /jpg|png/,
}); // get a directory tree

//---------------------------------------------------------------- Process methods
async function imagemin_optimize(src, dest) {
    return new Promise(async resolve => {
        try {
            const files = await imagemin(
                [src],
                dest,
                {
                    plugins: imPlugins
                }
            );
            resolve(files);
        }
        catch (e) {
            throw(e);
        }
    })
}

async function imagemin_pngConvert() {
    await imagemin(
        ['src/*.png'],
        'src',
        {
            plugins: [
                Imagemin_pngToJpeg({
                    quality: 100
                })
            ]
        }
    );
}

//---------------------------------------------------------------- ENTRY (IIFE, because of an async rule)
(async () => {
    let done = await recurseNode(tree);
    console.log(done);
})();