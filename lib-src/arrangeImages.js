/**
 *
 * @param arrayOfImages
 * @return []]
 */

/**
 * Uses `.filter()` and spreads to re-build the imgs arr however you need it to.
 * This is pseudo - idk what the devil props you have to work with. This just shows the filter+spread part of what you need.
 * @param arrayOfImages
 * @return {...*[]}
 */
const arrangeImgsLikeBlakeSaid = (arrayOfImages) => {
    // first filter: get the landscape imgs
    let imgsLandscape = arrayOfImages.filter(img => {
        return img.width >= img.height; // >= because there may be some oddball square ones that would get trashed otherwise
    });
    // second filter: get the portrait imgs
    let imgsPortrait = arrayOfImages.filter(img => {
        return img.height > img.width; // NOT >= because we already did those in imgsLandscape
    });

    // using the spread operator, combine the two filtered arrs the way you want
    return [
        ...imgsLandscape,
        ...imgsPortrait,
    ]
};

module.exports = {
    arrangeImgsLikeBlakeSaid
};

